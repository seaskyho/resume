import React, {Component} from 'react';
import ReactDOMServer from 'react-dom/server';
import {PDFExport} from '@progress/kendo-react-pdf';
import canvg from 'canvg';
import {color_theme} from '../styledComponents/themes'

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  faHome,
  faAt,
  faPhoneSquare,
  faCalendarAlt,
  faMapMarkerAlt,
  faChevronCircleDown,
  faAward,
  faUser,
  faCircle
} from '@fortawesome/free-solid-svg-icons'
import {faLinkedin, faBitbucket} from '@fortawesome/free-brands-svg-icons';

import {
  ContentArea, ContentWrapper,
  ContactIcon,
  AngleDownIcon, CalendarIcon,
  MapMarkerIcon,
  UserIcon, CircleIcon,
} from '../styledComponents/index';
import {content_area_theme} from '../styledComponents/themes';

import Header from "./Header"
import Education from "./Education"
import Projects from "./Projects"
import Skills from "./Skills"
import WorkHistory from "./WorkHistory"


class App extends Component {
  constructor() {
    super();
    this.iconsToConvert = [
      {
        icon: faAt,
        alt: 'envelope icon'
      },
      {
        icon: faPhoneSquare,
        alt: 'phone icon'
      },
      {
        icon: faHome,
        alt: 'igloo icon'
      },
      {
        icon: faLinkedin,
        alt: 'linkedin icon'
      },
      {
        icon: faBitbucket,
        alt: 'bitbucket icon'
      },
      {
        icon: faCalendarAlt,
        alt: 'calendar icon'
      },
      {
        icon: faMapMarkerAlt,
        alt: 'map-marker icon'
      }, {
        icon: faChevronCircleDown,
        alt: 'angle-down icon'
      }, {
        icon: faAward,
        alt: 'award icon'
      }, {
        icon: faUser,
        alt: 'user icon'
      },
      {
        icon: faCircle,
        alt: 'circle icon'
      }
    ];
    this.canvLoaded = false;
  }


  exportPDF = () => {
    this.resume.save();
  };

  convertSvgToImage = (arr) => {
    let canv = this.refs.canvas;
    const {
      ContactIconColor, CalendarIconColor, MapMarkerIconColor,
      AngleDownIconColor, AwardIconColor, UserIconColor, CircleIconColor
    } = color_theme;
    if (!this.canvLoaded) {
      this.canvLoaded = true;
      canv.getContext("2d");
      arr.forEach((d) => {
        let color;
        switch (d.alt) {
          case 'calendar icon':
            color = CalendarIconColor;
            break;
          case 'map-marker icon':
            color = MapMarkerIconColor;
            break;
          case 'angle-down icon':
            color = AngleDownIconColor;
            break;
          case 'award icon':
            color = AwardIconColor;
            break;
          case 'user icon':
            color = UserIconColor;
            break;
          case 'circle icon':
            color = CircleIconColor;
            break;
          default:
            // header(contact) icons color
            color = ContactIconColor;
        }
        let htmlString = ReactDOMServer.renderToStaticMarkup(
            <FontAwesomeIcon icon={d.icon} size={"3x"} style={{
              color: color,
              height: '500px',
              width: '500px'
            }}/>
        );
        canvg(canv, htmlString);
        d.icon = canv.toDataURL("image/png");
      });
      this.setState({});
    }
  };

  componentDidMount() {
    this.convertSvgToImage(this.iconsToConvert);
  }

  render() {
    const iconArray =
        this.canvLoaded && this.iconsToConvert.map((iconObject, index) => {
          switch (iconObject.alt) {
            case "calendar icon":
              return <CalendarIcon src={iconObject.icon} key={'img-' + index}
                                   alt={iconObject.alt}/>;
            case "map-marker icon":
              return <MapMarkerIcon src={iconObject.icon} key={'img-' + index}
                                    alt={iconObject.alt}/>;
            case 'angle-down icon':
              return <AngleDownIcon src={iconObject.icon} key={'img-' + index}
                                    alt={iconObject.alt}/>;
            case 'user icon':
              return <UserIcon src={iconObject.icon} key={'img-' + index}
                               alt={iconObject.alt}/>;
            case 'circle icon':
              return <CircleIcon src={iconObject.icon} key={'img-' + index}
                                 alt={iconObject.alt}/>;
            default:
              return <ContactIcon src={iconObject.icon} key={'img-' + index}
                                  alt={iconObject.alt}/>
          }
        });

    // const [icon_at, icon_phone, icon_home, icon_linkedin, icon_bitbucket,
    //   icon_calendar, icon_map_marker, icon_angle_down] = iconArray;

    const icon_at = iconArray[0];
    const icon_phone = iconArray[1];
    const icon_home = iconArray[2];
    const icon_linkedin = iconArray[3];
    const icon_bitbucket = iconArray[4];
    const icon_calendar = iconArray[5];
    const icon_map_marker = iconArray[6];
    const icon_angle_down = iconArray[7];
    const icon_award = iconArray[8];
    const icon_user = iconArray[9];
    const icon_circle = iconArray[10];

    return (
        <React.Fragment>
          {!this.canvLoaded && <canvas ref="canvas" style={{display: 'none'}}>
          </canvas>}
          <button onClick={this.exportPDF}>download</button>
          <PDFExport
              paperSize={'A4'}
              author={'Seasky'}
              avoidLinks={true}
              date={new Date()}
              fileName="resume.pdf"
              ref={(r) => this.resume = r}>
            <ContentWrapper theme={content_area_theme}>
              <ContentArea theme={content_area_theme}>

                {/*HEADER*/}
                <Header icon_at={icon_at} icon_phone={icon_phone}
                        icon_home={icon_home} icon_linkedin={icon_linkedin}
                        icon_bitbucket={icon_bitbucket}/>
                <hr style={{margin: '6px 0 6px 0'}}/>

                {/*BODY*/}
                <div className='row'>
                  <div className='col-6'>
                    {/*here goes EDUCATION and WORK HISTORY*/}
                    <Education icon_calendar={icon_calendar}
                               icon_map_marker={icon_map_marker}
                               icon_angle_down={icon_angle_down}
                               icon_award={icon_award}
                               icon_circle={icon_circle}/>
                    <WorkHistory icon_calendar={icon_calendar}
                                 icon_map_marker={icon_map_marker}
                                 icon_user={icon_user}
                                 icon_angle_down={icon_angle_down}
                                 icon_circle={icon_circle}/>
                  </div>
                  <div className='col-6'>
                    {/*here goes SKILL and PROJECTS*/}
                    <Skills icon_angle_down={icon_angle_down}/>
                    <Projects icon_calendar={icon_calendar}
                              icon_angle_down={icon_angle_down}
                              icon_circle={icon_circle}/>
                  </div>
                </div>

              </ContentArea>
            </ContentWrapper>
          </PDFExport>
          <div style={{height: '20px'}}/>
        </React.Fragment>
    );
  }
}

export default App;
