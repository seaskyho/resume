import React from 'react';

import {
  SectionTitle, SectionWrapper, UniName, DateText, TextNormal,
} from '../styledComponents/index';


export default function (props) {
  const {icon_calendar, icon_angle_down, icon_circle} = props;
  return <SectionWrapper>
    <SectionTitle>{icon_angle_down} Projects</SectionTitle>

    <UniName> Instagram iOS App (Team)</UniName>
    <div style={{position: 'relative', bottom: '3px'}}>
      {icon_calendar} <DateText> Sep 2018 – Oct 2018</DateText>
    </div>

    <TextNormal>
      {icon_circle} Designed and built the backend and database using AdonisJS
      and SQL, wrote RESTful APIs to handle frontend requests, implemented
      friend recommendation algorithm, used AWS S3 to serve user uploaded image
      files, and deployed the backend to Heroku.
    </TextNormal>

    <div style={{height: '12px'}}/>

    <UniName> Information Retrieval Based Question Answering System
      (Individual)</UniName>
    <div style={{position: 'relative', bottom: '3px'}}>
      {icon_calendar} <DateText>May 2018 – Jun 2018</DateText>
    </div>

    <TextNormal>
      {icon_circle} Built an IR-based factoid QA system, used techniques such
      as answer type detection, document ranking, named entity detection,
      noun-phrase chunking, dependency parsing, and topic modelling.
    </TextNormal>

    <div style={{height: '12px'}}/>

    <UniName> Authorship Verification for Academic Integrity
      (Individual) </UniName>
    <div style={{position: 'relative', bottom: '3px'}}>
      {icon_calendar} <DateText>Feb 2018 – Jun 2018</DateText>
    </div>

    <TextNormal>
      {icon_circle} Applied Character N-Gram (CNG) method (using Java) to
      PAN-2015 corpus, with the goal to validate CNG’s effectiveness in
      authorship verification task when texts are in cross-genre/topic/language.
    </TextNormal>

    <div style={{height: '12px'}}/>

    <UniName> Industrial Internet – Power Plant Monitoring (Team) </UniName>
    <div style={{position: 'relative', bottom: '3px'}}>
      {icon_calendar} <DateText>Sep 2015 – Dec 2015</DateText>
    </div>

    <TextNormal>
      {icon_circle} Used MATLAB to analyze KPI of power plant’s heat
      exchangers, MySQL to store heat exchangers’ performance data, and AxureRP
      to build up website GUI where data can be shared anytime. Achieved
      real-time monitoring of heat exchangers.
    </TextNormal>

  </SectionWrapper>
};