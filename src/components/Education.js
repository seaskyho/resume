import React from 'react';
import {
  SectionTitle, SectionWrapper,
  UniName, DateWrapper, DateText,
  MapWrapper, MapText, TextStrong, TextNormal,
} from '../styledComponents/index';


export default function (props) {
  const {icon_calendar, icon_map_marker, icon_angle_down} = props;
  return (
      <SectionWrapper>
        <SectionTitle>{icon_angle_down} Education</SectionTitle>
        <UniName>
          University of Melbourne
        </UniName>
        <div className='row' style={{position: 'relative', bottom: '5px'}}>
          <DateWrapper className='col-6'>
            {icon_calendar} <DateText>Feb 2017 - Dec 2018</DateText>
          </DateWrapper>
          <MapWrapper className='col-6'>
            {icon_map_marker} <MapText>Melbourne, VIC</MapText>
          </MapWrapper>
        </div>

        <div className='row'>
          <TextStrong className='col-2'>
            Major
          </TextStrong>
          <TextNormal className='col-10'>
            Master of Information Technology
          </TextNormal>
        </div>

        <div className='row'>
          <TextStrong className='col-2'>
            Grade
          </TextStrong>
          <TextNormal className='col-10'>
            87.56/100.00
          </TextNormal>
        </div>

        <div className='row'>
          <TextStrong className='col-2'>
            Awards
          </TextStrong>
          <TextNormal className='col-10'>
            Dean’s Honours List (top5% academic, 2 times)<br/>
            John Balfour Memorial Scholarship (2 times)
          </TextNormal>
        </div>

        <div style={{height: '12px'}}/>

        <UniName>
          Shanghai Jiao Tong University
        </UniName>
        <div className='row' style={{position: 'relative', bottom: '5px'}}>
          <DateWrapper className='col-6'>
            {icon_calendar} <DateText>Sep 2012 – Aug 2016</DateText>
          </DateWrapper>
          <MapWrapper className='col-6'>
            {icon_map_marker} <MapText>Shanghai</MapText>
          </MapWrapper>
        </div>

        <div className='row'>
          <TextStrong className='col-2'>
            Major
          </TextStrong>
          <TextNormal className='col-10'>
            Bachelor of Mechanical Engineering
          </TextNormal>
        </div>

        <div className='row'>
          <TextStrong className='col-2'>
            Grade
          </TextStrong>
          <TextNormal className='col-10'>
            3.55/4.00
          </TextNormal>
        </div>

        <div className='row'>
          <TextStrong className='col-2'>
            Awards
          </TextStrong>
          <TextNormal className='col-10'>
            Dean’s List (GPA≥3.50 per semester, 7 times)<br/>
            SJTU Merit Student
          </TextNormal>
        </div>
      </SectionWrapper>
  )
};