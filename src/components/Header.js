import React from 'react';
import {
  MyName, ContactWrapper, ContactInfo,
} from '../styledComponents/index';

export default function (props) {
  const {icon_at, icon_phone, icon_home, icon_linkedin, icon_bitbucket} = props;

  return <React.Fragment>
    <div>
      <MyName>Haitian He</MyName>
    </div>
    <ContactWrapper>
      {icon_at}
      <ContactInfo>seaskyho@gmail.com</ContactInfo>
    </ContactWrapper>

    <ContactWrapper>
      {icon_phone}
      <ContactInfo>(+61) 0490 770 131</ContactInfo>
    </ContactWrapper>

    <ContactWrapper>
      {icon_home}
      <ContactInfo> Unit 5, 130 Alma Rd, St Kilda East, VIC 3183</ContactInfo>
    </ContactWrapper>

    <ContactWrapper>
      {icon_linkedin}
      <ContactInfo>linkedin.com/in/haitian-he-3a529a119</ContactInfo>
    </ContactWrapper>

    <ContactWrapper>
      {icon_bitbucket}
      <ContactInfo>bitbucket.org/seaskyho/ </ContactInfo>
    </ContactWrapper>

  </React.Fragment>
};