import React from 'react';


import {
  SectionTitle,
  SectionWrapper,
  SkillCategory,
  SkillItem
} from '../styledComponents/index';


export default function (props) {
  const {icon_angle_down} = props;
  return (
      <SectionWrapper>
        <SectionTitle>{icon_angle_down} Skills</SectionTitle>

        <SkillCategory style={{marginLeft: '3px'}}>Web</SkillCategory>
        <SkillItem>HTML</SkillItem>
        <SkillItem>CSS</SkillItem>
        <SkillItem>Javascript</SkillItem>
        <SkillItem>ReactJS</SkillItem>
        <SkillItem>Redux</SkillItem>
        <SkillItem>Bootstrap</SkillItem>
        <SkillItem>Styled Component</SkillItem>
        <SkillItem>SASS</SkillItem>
        <SkillItem>ExpressJS</SkillItem>
        <SkillItem>NodeJS</SkillItem>
        <SkillItem>AdonisJS</SkillItem>
        <SkillItem>RESTful API</SkillItem>
        <SkillItem>Heroku</SkillItem>
        <SkillItem>Unit Test</SkillItem>
        <SkillItem>Enzyme</SkillItem>
        <SkillItem>Jest</SkillItem>
        <SkillItem>AWS S3</SkillItem>
        <SkillItem>jQuery</SkillItem>
        <SkillItem>JWT</SkillItem>

        <SkillCategory>Database</SkillCategory>
        <SkillItem>MySQL</SkillItem>
        <SkillItem>Knex.js</SkillItem>
        <SkillItem>MangoDB</SkillItem>
        <SkillItem>Mongoose</SkillItem>

        <SkillCategory>Data Science</SkillCategory>
        <SkillItem>NLP</SkillItem>
        <SkillItem>NLTK</SkillItem>
        <SkillItem>spaCy</SkillItem>
        <SkillItem>Scikit-Learn</SkillItem>
        <SkillItem>Gensim</SkillItem>
        <SkillItem>Machine Learning</SkillItem>
        <SkillItem>Text Parsing</SkillItem>

        <SkillCategory>Programming Language</SkillCategory>
        <SkillItem>Python</SkillItem>
        <SkillItem>Java</SkillItem>
        <SkillItem>Haskell</SkillItem>
        <SkillItem>Prolog</SkillItem>

        <SkillCategory>General</SkillCategory>
        <SkillItem>GIT</SkillItem>
        <SkillItem>Agile</SkillItem>
        <SkillItem>Trello</SkillItem>
        <SkillItem>Slack</SkillItem>
        <SkillItem>Regex</SkillItem>
        <SkillItem>Markdown</SkillItem>
        <SkillItem>Bash</SkillItem>
        <SkillItem>Photoshop</SkillItem>
      </SectionWrapper>
  )
};