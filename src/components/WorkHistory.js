import React from 'react';

import {
  SectionTitle, SectionWrapper,
  UniName, DateWrapper, DateText,
  MapWrapper, MapText, TextNormal,
  UserText,
} from '../styledComponents/index';


export default function (props) {
  const {icon_calendar, icon_map_marker, icon_angle_down, icon_user, icon_circle} = props;
  return (
      <SectionWrapper>
        <SectionTitle>{icon_angle_down} Work History</SectionTitle>

        <UniName>RESORTer {icon_user}
          <UserText> Intern & Part Time - Web Developer
          </UserText>
        </UniName>

        <div className='row' style={{position: 'relative', bottom: '5px'}}>
          <DateWrapper className='col-6'>
            {icon_calendar} <DateText>July 2018 – Apr 2019</DateText>
          </DateWrapper>
          <MapWrapper className='col-6'>
            {icon_map_marker} <MapText>Melbourne, VIC</MapText>
          </MapWrapper>
        </div>

        <TextNormal>
          {icon_circle} Full-stack web app developer. Technical stack: ReactJS
          + Redux + AdonisJS (NodeJS) + MySQL + Heroku + Git.<br/>

          {icon_circle} Worked in agile
          lifecycle from requirement analysis to development. Used Trello
          for process tracking. <br/>

          {icon_circle} In charge of database design and maintenance. <br/>

          {icon_circle} Adopted Styled Component, CSS3, and Bootstrap to draw
          the UI. Implemented pages for users to book skiing trips.
          Handled user registration, authentication, sign in/out.

        </TextNormal>

        <div style={{height: '12px'}}/>

        <UniName>George Robotics {icon_user}
          <UserText> Intern - Mechatronics
          </UserText>
        </UniName>

        <div className='row' style={{position: 'relative', bottom: '5px'}}>
          <DateWrapper className='col-6'>
            {icon_calendar} <DateText>July 2017 – Sep 2017</DateText>
          </DateWrapper>
          <MapWrapper className='col-6'>
            {icon_map_marker} <MapText>Melbourne, VIC</MapText>
          </MapWrapper>
        </div>

        <TextNormal>
          {icon_circle} Designed and built a two-wheel self-balancing robot
          using
          Micro-python Pyboard microcontroller, gyroscope, accelerometer,
          stepper motor, and PID control.
        </TextNormal>

        <div style={{height: '12px'}}/>

        <UniName>General Electric {icon_user}
          <UserText> Intern - Consultant
          </UserText>
        </UniName>

        <div className='row' style={{position: 'relative', bottom: '5px'}}>
          <DateWrapper className='col-6'>
            {icon_calendar} <DateText>Feb 2016 – May 2016</DateText>
          </DateWrapper>
          <MapWrapper className='col-6'>
            {icon_map_marker} <MapText>Shanghai</MapText>
          </MapWrapper>
        </div>

        <TextNormal>
          {icon_circle} Participated in a Wind Turbine Modelling and Simulation
          project,
          conducted simulation tests using Flex5 under various turbine
          configurations, and then wrote MATLAB codes to process generated data
          files with the aim to maximize power generation, meanwhile minimizing
          turbine fatigue.
        </TextNormal>

        <div style={{height: '12px'}}/>

        <UniName>Shanghai Jiao Tong University {icon_user}
          <UserText> Tutor
          </UserText>
        </UniName>

        <div className='row' style={{position: 'relative', bottom: '5px'}}>
          <DateWrapper className='col-6'>
            {icon_calendar} <DateText>Sep 2015 – Dec 2015</DateText>
          </DateWrapper>
          <MapWrapper className='col-6'>
            {icon_map_marker} <MapText>Shanghai</MapText>
          </MapWrapper>
        </div>

        <TextNormal>
          {icon_circle} Tutor for subject: Mechanical Behavior of
          Materials <br/>
          {icon_circle} Delivered recitation classes, opened office hours,
          supervised exams,
          tutored labs, and graded students’ homework and exam papers.
        </TextNormal>

      </SectionWrapper>
  )
};