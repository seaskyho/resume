import styled from 'styled-components'
import {color_theme} from './themes'

const {
  MyNameColor, ContactInfoColor, ContactIconColor, SectionTitleColor,
  CalendartextColor, MapTextColor, UserTextColor, SkillItemBGColor,
  SkillItemBorderColor
} = color_theme;
/*
 Paper Settings
 */
export const ContentWrapper = styled.div`
   height: ${props => props.theme.height}px;
   width:${props => props.theme.width}px;
   margin: auto;
   padding: ${props => props.theme.padding_y}px ${props => props.theme.padding_x}px;
   background-color: white;
   box-sizing: border-box;
`;

export const ContentArea = styled.div`
  height: ${props => props.theme.height - 2 * props.theme.padding_y}px;
  width:${props => props.theme.width - 2 * props.theme.padding_x}px;
  background-color: white;
  overflow-y: hidden;
  font-size: 0.7rem;
`;

/*
 Header
 */
export const MyName = styled.span`
  font-size: 1.5rem;
  padding: 0 40px 0 0;
  font-family: "CambriaBold";
  color: ${MyNameColor};
`;
export const ContactWrapper = styled.div`
  display: inline-block;
  margin-right: 13px;
`;

export const ContactIcon = styled.img`
 height: 11px;
 width: 11px;
 margin-right: 3px;
 position: relative;
 bottom: 1px;
`;

export const ContactInfo = styled.span`
 color: ${ContactInfoColor};
 font-size:0.3rem;
`;

/*
  Sections
 */

export const SectionWrapper = styled.div`
  margin-bottom:14px;
  line-height: 1.3;
`;

export const AngleDownIcon = styled.img`
 height: 13px;
 width: 13px;
 margin: 0 4px 0  0;
 position:relative;
 bottom: 1px;
`;

export const SectionTitle = styled.div`
  font-size: 1rem;
  color:${SectionTitleColor};
  font-family:'CambriaBold';
  margin-bottom: 3px;
`;


/*
 Content
 */
export const UniName = styled.div`
  font-size:0.8rem;
  font-family:'CambriaBold';
`;

export const DateWrapper = styled.div`
  position:relative;
  top:2px;
`;

export const CalendarIcon = styled.img`
 height: 9px;
 width: 9px;
 margin: 0 4px 0  0;
`;

export const DateText = styled.span`
  color:${CalendartextColor};
  font-size: 0.5rem;
  position:relative;
  top:1px;
`;

export const MapWrapper = styled.div`
  
  position:relative;
  top:2px;
`;

export const MapMarkerIcon = styled.img`
 height: 9px;
 width: 9px;
 margin: 0 4px 0  0;
`;

export const MapText = styled.span`
  color:${MapTextColor};
  font-size: 0.5rem;
  position:relative;
  top:1px;
`;

export const AwardIcon = styled.img`
 height: 9px;
 width: 9px;
 margin: 0 4px 0  0;
`;

export const TextStrong = styled.span`
  font-family: 'CambriaBold';
  font-size:0.6rem;
`;

export const TextNormal = styled.span`
  font-family:'CambriaRegular';
  font-size:0.6rem;
`;

export const UserIcon = styled.img`
 height: 9px;
 width: 9px;
 margin: 0 4px 0  9px;
 position:relative;
 bottom: 1px;
`;

export const UserText = styled.span`
  color:${UserTextColor};
  font-size: 0.5rem;
  position:relative;
  bottom:1px;
  font-family: 'CambriaRegular';
`;

export const CircleIcon = styled.img`
 height: 4px;
 width: 4px;
 margin: 0 4px 0  0;
 position:relative;
 bottom: 1px;
`;

export const SkillCategory = styled.span`
  font-family: 'CambriaBold';
  font-size: 0.7rem;
  color:${ContactIconColor};
  margin: 0 3px 1px 10px;
`;

export const SkillItem = styled.div`
  display:inline-block;
  border: 1px solid ${SkillItemBorderColor};
  background-color: ${SkillItemBGColor};
  border-radius: 3px;
  padding: 0 3px;
  font-family: 'CambriaRegular';
  font-size: 0.6rem;
  margin: 0 2px 1px 0;
`;




