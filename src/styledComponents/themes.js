const most_icon_color_option1 = "#6D99B0";
const most_icon_color_option2 = "#2C7EB4";


export const content_area_theme = {
  height: Math.round(11.69 * 72), // inch * 72 = pixel
  width: Math.round(8.27 * 72),
  padding_x: 30, // pixel
  padding_y: 30, // pixel
};

export const color_theme = {
  MyNameColor: '#222F67',
  ContactIconColor: "#005696",
  ContactInfoColor: '#131A39',
  AngleDownIconColor: '#222F67',
  SectionTitleColor: '#222F67',
  SectionDividerColor: "#BECCCE",
  CalendarIconColor: most_icon_color_option2,
  CalendartextColor: most_icon_color_option2,
  MapMarkerIconColor: most_icon_color_option2,
  MapTextColor: most_icon_color_option2,
  AwardIconColor: most_icon_color_option2,
  UserIconColor: most_icon_color_option2,
  UserTextColor: most_icon_color_option2,
  CircleIconColor: '#000000',
  SkillItemBorderColor: "#E1E4E6",
  SkillItemBGColor: '#F3F7FA',
};