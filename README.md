# React Resume App
---

### Introduction

Yes, that's right, I wrote an React App to build my resume. Exciting! 

I found Office Word is not convenient enough when you want to have precise control over your resume layout. Meanwhile, Adobe InDesign seems like a perfect tool for professional page design, but it looks daunting for a beginner to learn. 

So, why not use what I'm good at, which is my web design skills, to build my own resume? Luckily, there are package supports for PDF file design with React. The one that I used is [Progress KendoReact](https://www.telerik.com/kendo-react-ui/), and there is also a very nice [tutorial](https://blog.usejournal.com/lets-make-a-resume-in-react-2c9c5540f51a) about how to use this tool to design your resume.

### Packages List
Here is a list of packages used in this app:

* fontawesome
* bootstrap
* styled-components
* canvg
* progress


### How to Run
1. Download the repo by `git clone https://seaskyho@bitbucket.org/seaskyho/resume.git`
2. Go into the main folder `cd yourLocalRepoName`
3. Install all the dependencies `npm i`
4. Then start the app `npm start`
5. On your browser, click the **Download** button, then the resume is in your local machine. Easy!



